//
//  FrontNavigationController.h
//  Universal
//
//  Created by Mu-Sonic on 25/10/2015.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavbarGradientView.h"

@interface FrontNavigationController : UITabBarController

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

+(UIViewController *) createViewController:(NSArray*) item withStoryboard:(UIStoryboard *) storyboard;

@end
