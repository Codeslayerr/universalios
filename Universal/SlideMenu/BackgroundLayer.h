//
//  BackgroundLayer.h
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>


@interface BackgroundLayer : NSObject

+(CAGradientLayer*) colorGradient;

@end
