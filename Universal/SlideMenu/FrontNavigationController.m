//
//  FrontNavigationController.m
//  Universal
//
//  Created by Mu-Sonic on 25/10/2015.
//  Copyright © 2016 Sherdle. All rights reserved.
//

#import "SWRevealViewController.h"
#import "FrontNavigationController.h"
#import "TabNavigationController.h"
#import "AppDelegate.h"
#import "Config.h"

#import "RadioViewController.h"
#import "TvViewController.h"
#import "FacebookViewController.h"
#import "MapsViewController.h"
#import "RssViewController.h"
#import "YoutubeViewController.h"
#import "TumblrViewController.h"
#import "WebViewController.h"
#import "WordpressViewController.h"
#import "InstagramViewController.h"
#import "PinterestViewController.h"
#import "TwitterViewController.h"

#import "MoreViewController.h"

@implementation FrontNavigationController
{
    UIColor *prevShadowColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    prevShadowColor = self.revealViewController.frontViewShadowColor;
    self.tabBar.translucent = NO;
    self.tabBar.backgroundColor = [UIColor lightGrayColor];

    if (!_selectedIndexPath) {
        _selectedIndexPath  = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    NSArray *sectionArray = [Config config];
    NSArray *item = [[sectionArray objectAtIndex: _selectedIndexPath.section] objectAtIndex:(_selectedIndexPath.row + 1)];
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    if ([[item objectAtIndex: 1] isKindOfClass:[NSArray class]]){
    
        NSArray *items = [item objectAtIndex: 1];
        if ( [[item objectAtIndex: 1] count] <= 5) {
            for (id subItem in items) {
                TabNavigationController *controller = [self controllerFromItem:subItem];
                [viewControllers addObject:controller];
            }
        } else {
            for (id subItem in [items subarrayWithRange:NSMakeRange(0, 4)]) {
                TabNavigationController *controller = [self controllerFromItem:subItem];
                [viewControllers addObject:controller];
            }
            
            TabNavigationController *controller = [self moreControllerFromItems:[items subarrayWithRange:NSMakeRange(4, [items count] - 4)]];
            [viewControllers addObject:controller];
        }
        
    } else {
        TabNavigationController *controller = [self controllerFromItem:item];
        controller.viewControllers[0].hidesBottomBarWhenPushed = true;
        [viewControllers addObject:controller];
    }
    
    self.viewControllers = viewControllers;

}

- ( TabNavigationController *) controllerFromItem: (NSArray *) item{
    UIViewController *controller = [FrontNavigationController createViewController:item withStoryboard:self.storyboard];
    
    UIImage *tabImage = nil;
    if ([item count] >= 4 && [item objectAtIndex: 3] != nil)
        tabImage = [UIImage imageNamed:[item objectAtIndex: 3]];
    
    UITabBarItem *tabItem = [[UITabBarItem alloc] initWithTitle:[item objectAtIndex:0] image:tabImage selectedImage:tabImage];
    controller.tabBarItem = tabItem;

    TabNavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabNavigationController"];
    
    return [navigationController initWithRootViewController:controller];
}

- ( TabNavigationController *) moreControllerFromItems: (NSArray *) items{
    MoreViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
    controller.items = items;
    controller.title = NSLocalizedString(@"more", nil);
    
    UIImage *tabImage = [UIImage imageNamed:@"more"];
    
    UITabBarItem *tabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"more", nil) image:tabImage selectedImage:tabImage];
    controller.tabBarItem = tabItem;
    
    TabNavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabNavigationController"];
    
    return [navigationController initWithRootViewController:controller];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

+(UIViewController *) createViewController:(NSArray*) item withStoryboard:(UIStoryboard *) storyboard{
    NSString *SOCIAL_ITEMS_NAME = [item objectAtIndex: 0];
    NSString *SOCIAL_ITEMS_TYPE = [item objectAtIndex: 1];
    NSArray *SOCIAL_ITEMS_PARAMS = [item objectAtIndex: 2];
    
    UIViewController *controller;
    
    if ([SOCIAL_ITEMS_TYPE isEqual:@"WORDPRESS"]) {
    
        controller = (WordpressViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WordpressViewController"];
        
        ((WordpressViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"YOUTUBE"]){
        controller = (YoutubeViewController *)  [storyboard instantiateViewControllerWithIdentifier:@"YoutubeViewController"];
        
        ((YoutubeViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"TUMBLR"]) {
        controller = (TumblrViewController *) [storyboard instantiateViewControllerWithIdentifier:@"TumblrViewController"];
        
        ((TumblrViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"MAPS"]) {
        controller = (MapsViewController *) [storyboard instantiateViewControllerWithIdentifier:@"MapsViewController"];
        
        ((MapsViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqual:@"RADIO"]) {
        controller = (RadioViewController *) [storyboard instantiateViewControllerWithIdentifier:@"RadioViewController"];
        
        ((RadioViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
        ((RadioViewController *)controller).navTitle = SOCIAL_ITEMS_NAME;
    } else if ([SOCIAL_ITEMS_TYPE isEqual:@"TV"]) {
        controller = (TvViewController *) [storyboard instantiateViewControllerWithIdentifier:@"TvViewController"];
        
        ((TvViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"WEB"]) {
        controller = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        
        ((WebViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"RSS"]) {
        controller = (RssViewController *) [storyboard instantiateViewControllerWithIdentifier:@"RssViewController"];
        
        ((RssViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"TWITTER"]) {
        controller = (TwitterViewController *) [storyboard instantiateViewControllerWithIdentifier:@"TwitterViewController"];
        
        ((TwitterViewController *)controller).screenName = SOCIAL_ITEMS_PARAMS[0];
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"FACEBOOK"]) {
        controller = (FacebookViewController *) [storyboard instantiateViewControllerWithIdentifier:@"FacebookViewController"];
        
        ((FacebookViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    }  else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"INSTAGRAM"]) {
        controller = (InstagramViewController *) [storyboard instantiateViewControllerWithIdentifier:@"InstagramViewController"];
        
        ((InstagramViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    } else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"PINTEREST"]) {
        controller = (PinterestViewController *) [storyboard instantiateViewControllerWithIdentifier:@"PinterestViewController"];
        
        ((PinterestViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    }   else if ([SOCIAL_ITEMS_TYPE isEqualToString:@"SOUNDCLOUD"]) {
        controller = (SoundCloudViewController *) [storyboard instantiateViewControllerWithIdentifier:@"SoundCloudViewController"];
        
        ((SoundCloudViewController *)controller).params = SOCIAL_ITEMS_PARAMS;
    }
    
    controller.title = SOCIAL_ITEMS_NAME;
    
    return controller;
}

@end
