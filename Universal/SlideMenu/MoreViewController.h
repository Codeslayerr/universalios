//
//  MoreViewController.h
//  Universal
//
//  Copyright © 2016 Sherdle. All rights reserved.
//

@import UIKit;

@interface MoreViewController : UITableViewController

@property NSArray *items;

@end
