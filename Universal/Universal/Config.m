//
//  Config.m
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//
//  INFO: From this file you can edit your app's main content.
//

#import "Config.h"

@implementation Config {
    
}

+ (NSArray *)config {
    NSMutableArray *sections = [NSMutableArray array];
    
    //-- Please only edit below this line.
    
    //TODO re-test interstitial
    
    [sections addObject: @[@"Social",
                           @[@"Blog", @[
                                 @[@"Recent", @"WORDPRESS", @[@"en.blog.wordpress.com", @""], @"more"],
                                 @[@"Themes", @"WORDPRESS", @[@"en.blog.wordpress.com", @"themes"], @"more"],
                                 @[@"Security", @"WORDPRESS", @[@"en.blog.wordpress.com", @"security"], @"more"],
                                 @[@"Events", @"WORDPRESS", @[@"en.blog.wordpress.com", @"events"], @"more"]
                                 ]],
                           @[@"Twitter", @"TWITTER", @[@"Google"]],
                           @[@"Instagram", @"INSTAGRAM", @[@"2757038469"]],
                           @[@"Facebook", @"FACEBOOK", @[@"104958162837"]],
                           @[@"Youtube", @"YOUTUBE", @[@"UUK8sQmJBp8GCxrOtXWBpyEA", @"UCK8sQmJBp8GCxrOtXWBpyEA"]],
                           @[@"SoundCloud", @"SOUNDCLOUD", @[@"13568105", @"user"]],
                           @[@"Tumblr", @"TUMBLR", @[@"android-backgrounds"]],
                           @[@"Pinterest", @"PINTEREST", @[@"215680338343615778"]],
                           ]];
    
    [sections addObject: @[@"Streaming",
                           @[@"Radio 3FM", @"RADIO", @[@"http://yp.shoutcast.com/sbin/tunein-station.m3u?id=709809"]],
                           @[@"ABC Live", @"TV", @[@"http://abclive.abcnews.com/i/abc_live4@136330/index_1200_av-b.m3u8"]],
                           ]];
    
    [sections addObject: @[@"Other",
                           @[@"Feed", @"RSS", @[@"https://blog.google/rss/"]],
                           @[@"WebView", @"WEB", @[@"http://wikipedia.com"]],
                           @[@"Local Page", @"WEB", @[@"index.html"]],
                           @[@"Map", @"MAPS", @[@"-33.84",@"151.21",@"14", @"Event Location", @"This is where the super cool event takes place."]],
                           @[@"WP Disqus", @"WORDPRESS", @[@"http://androidpolice.com/api/", @"", @"http://androidpolice.disqus.com/;androidpolice;$d http://www.androidpolice.com/?p=$d"]],
                           @[@"Custom", @"CUSTOM", @[@"sms://0623214444"]],
                           ]];
    
    //---- Please only edit above this line
    
    return [NSArray arrayWithArray:sections];
}
@end
