//
//  AppDelegate.h
//
//  Copyright (c) 2016 Sherdle. All rights reserved.
//
//  INFO: In this file you can edit some of your apps main properties, like API keys
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "RearTableViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SoundCloudPlayerController.h"
#import "RadioViewController.h"
#import <OneSignal/OneSignal.h>
#import "CJPAdMobHelper.h"

//START OF CONFIGURATION

/**
 * Layout options
 */ 
#define APP_DRAWER_HEADER YES
#define APP_THEME_COLOR [UIColor colorWithRed:40.0f/255.0f  green:155.0f/255.0f  blue:222.0f/255.0f  alpha:1.0]
#define MENU_BACKGROUND_COLOR_1 [UIColor colorWithRed:0.11 green:0.38 blue:0.94 alpha:1.0]
#define MENU_BACKGROUND_COLOR_2 [UIColor colorWithRed:0.11 green:0.73 blue:0.85 alpha:1.0]

/**
 * About / Texts
 **/
#define NO_CONNECTION_TEXT @"We weren't able to connect to the server. Make sure you have a working internet connection."
#define ABOUT_TEXT @"Thank you for downloading our app! \n\nIf you need any help, press the button below to visit our support."
#define ABOUT_URL @"http://yoursupporturl.com"

/**
 * Monetization
 **/
#define INTERSTITIAL_INTERVAL 5
#define ADMOB_INTERSTITIAL_ID @""
#define BANNER_ADS_ON false
#define ADMOB_UNIT_ID @""

#define IN_APP_PRODUCT @""

/**
 * API Keys
 **/
#define ONESIGNAL_APP_ID @""

#define MAPS_API_KEY @""

#define YOUTUBE_CONTENT_KEY @""

#define TWITTER_API @""
#define TWITTER_API_SECRET @""
#define TWITTER_TOKEN @""
#define TWITTER_TOKEN_SECRET @""

#define FACEBOOK_ACCESS_TOKEN @""
#define INSTAGRAM_ACCESS_TOKEN @""
#define PINTEREST_ACCESS_TOKEN @""

#define SOUNDCLOUD_CLIENT @""

/**
 * Other
 */
#define OPEN_IN_BROWSER false

//END OF CONFIGURATION

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) AVPlayer *player;

@property (strong, nonatomic) OneSignal *oneSignal;

@property (nonatomic) int interstitialCount;


//Keeping a reference to controller that is currently playing audio. 
@property (strong, nonatomic) UIViewController* activePlayerController;
- (void) setActivePlayingViewController: (UIViewController *) active;
- (UIViewController *) activePlayingViewController;
- (void) closePlayerWithObserver: (NSObject *) observer;

//Utility methods
- (BOOL) shouldShowInterstitial;
+ (BOOL) hasPurchased;
+ (void) openUrl: (NSString *) url withNavigationController: (UINavigationController *) navController;
@end
